package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type ApiConfig struct {
	Hostname string
	Port     string
}

type DbConfig struct {
	Dialect  string
	Host     string
	Port     string
	DbName   string
	Username string
	Password string
}

type Person struct {
	gorm.Model

	Name  string
	Email string `gorm:"typevarchar(100);unique_index"`
	Books []Book
}

type Book struct {
	gorm.Model

	Title      string
	Author     string
	CallNumber string `gorm:"unique_index"`
	PersonID   int
}

var db *gorm.DB
var err error

func main() {
	// Loading Environment Variables
	dbConfig := readDbConfig()
	apiConfig := readApiConfig()

	// Database connection string
	dbUri := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s port=%s", dbConfig.Host, dbConfig.Username, dbConfig.DbName, dbConfig.Password, dbConfig.Port)

	fmt.Println(dbUri)
	// Opening db connection to database
	db, err = gorm.Open(dbConfig.Dialect, dbUri)

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Database successfully connected")
	}

	// Close db automatically when done
	defer db.Close()

	// Auto create table if not existing
	db.AutoMigrate(&Person{})
	db.AutoMigrate(&Book{})

	// API Routes
	router := mux.NewRouter()
	router.HandleFunc("/person", personGet).Methods("GET")
	router.HandleFunc("/person/{id}", personGetById).Methods("GET")
	router.HandleFunc("/person/{id}", personDeleteById).Methods("DELETE")
	// router.HandleFunc("/person/{id}", updatePeopleById).Methods("PUT")
	router.HandleFunc("/person", personCreate).Methods("POST")

	router.HandleFunc("/book", bookGet).Methods("GET")
	router.HandleFunc("/book/{id}", bookGetById).Methods("GET")
	router.HandleFunc("/book", bookCreate).Methods("POST")
	router.HandleFunc("/book/{id}", bookUpdate).Methods("PUT")
	router.HandleFunc("/book/{id}", bookDeleteById).Methods("DELETE")

	//router.HandleFunc("/person")
	//router.HandleFunc()

	apiAddress := fmt.Sprintf("%s:%s", apiConfig.Hostname, apiConfig.Port)
	fmt.Println("Starting Http Api ", apiAddress)

	serveResult := http.ListenAndServe(apiAddress, router)
	if serveResult != nil {
		log.Fatalln("Failed getting http result:", serveResult)
	}

	fmt.Println("Http api services started successfully")

}

// Person Controllers -------------------------------------
func personGet(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Get Person from db
	var personList []Person
	db.Find(&personList)

	// Encode to json
	json.NewEncoder(w).Encode(&personList)

}

func personGetById(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Get params
	params := mux.Vars(r)

	// Get person from db
	var onePerson Person
	var bookList []Book
	db.FirstOrInit(&onePerson, params["id"])
	db.Model(&onePerson).Related(&bookList)

	onePerson.Books = bookList

	// Encode to json
	json.NewEncoder(w).Encode(&onePerson)
}

func personDeleteById(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Init var
	var onePerson Person

	// Get params
	params := mux.Vars(r)

	// Get person from db
	db.First(&onePerson, params["id"])
	db.Delete(&onePerson)

	json.NewEncoder(w).Encode(&onePerson)
}

func personCreate(w http.ResponseWriter, r *http.Request) {

	// Set content heaer
	w.Header().Set("Content-Type", "application/json")

	// Request Person
	var requestPerson Person
	json.NewDecoder(r.Body).Decode(&requestPerson)

	// Create in db
	createResult := db.Create(&requestPerson)
	err = createResult.Error

	if err != nil {
		json.NewEncoder(w).Encode(err)
	} else {
		json.NewEncoder(w).Encode(&requestPerson)
	}
}

// Books Controllers -------------------------------------
func bookGet(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Get books list
	var booksList []Book
	db.Find(&booksList)

	// Output json
	json.NewEncoder(w).Encode(&booksList)
}

func bookGetById(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Get params
	params := mux.Vars(r)

	// Get by id
	var oneBook Book
	db.FirstOrInit(&oneBook, params["id"])

	// Output json
	json.NewEncoder(w).Encode(&oneBook)
}
func bookUpdate(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Get params
	params := mux.Vars(r)

	// Get request
	var requestBook Book
	json.NewDecoder(r.Body).Decode(&requestBook)

	// Get by id
	var oneBook Book
	db.First(&oneBook, params["id"])

	oneBook.Title = requestBook.Title
	oneBook.Author = requestBook.Author
	oneBook.CallNumber = requestBook.CallNumber
	oneBook.PersonID = requestBook.PersonID

	saveResult := db.Save(oneBook)
	err := saveResult.Error

	// Output json
	if err != nil {
		json.NewEncoder(w).Encode(err)
	} else {
		json.NewEncoder(w).Encode(&requestBook)
	}
}

func bookCreate(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Get request
	var requestBook Book
	json.NewDecoder(r.Body).Decode(&requestBook)

	// Create and get result
	createdResult := db.Create(&requestBook)
	err = createdResult.Error

	// Output json
	if err != nil {
		json.NewEncoder(w).Encode(err)
	} else {
		json.NewEncoder(w).Encode(&requestBook)
	}
}
func bookDeleteById(w http.ResponseWriter, r *http.Request) {

	// Set content header
	w.Header().Set("Content-Type", "application/json")

	// Get params
	params := mux.Vars(r)

	// Delete book
	var oneBook Book
	db.First(&oneBook, params["id"])
	db.Delete(&oneBook)

	// Output json
	json.NewEncoder(w).Encode(&oneBook)
}

// Config controllers -------------------------------------
// Function readDbConfig to return as object DbConfig
func readDbConfig() DbConfig {

	file, _ := os.Open("dbConfig.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	var dbConfig = DbConfig{}
	err := decoder.Decode(&dbConfig)

	if err != nil {
		log.Fatal("Error read dbconfig:", err)
	}

	return dbConfig
}

// Function readApiConfig to return as object ApiConfig
func readApiConfig() ApiConfig {

	file, _ := os.Open("apiConfig.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	var apiConfig = ApiConfig{}
	err := decoder.Decode(&apiConfig)

	if err != nil {
		log.Fatal("Error read dbconfig:", err)
	}

	return apiConfig
}
